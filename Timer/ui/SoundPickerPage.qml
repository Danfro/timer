import QtQuick 2.9
import Lomiri.Components 1.3
import QtMultimedia 5.6
import Qt.labs.folderlistmodel 2.1


Rectangle {
    id: root_soundpickerpage

    property alias audio_play: audio_play
    property string temp_sound

    width: root_soundpickerpage.width > root_soundpickerpage.height && !isHigh ? root_soundpickerpage.width/2 : root_soundpickerpage.width
    //height: stop_play_item.height + timersound_listview.height + systemsound_listview.height

    color: main_back_color

    PageHeader {
        id: main_header

        StyleHints {backgroundColor: main_back_color; foregroundColor: top_text_color}
        leadingActionBar.actions: Action {
            text: "close"
            iconName: "close"
            onTriggered: {
                audio_play.stop()
                sound_ldr.source = ""
            }
        }

        Button {
            id: stop_play_button

            property bool isPlaying: audio_play.playbackState == Audio.PlayingState

            width: Math.min(parent.width - units.gu(4), units.gu(22))
            anchors.centerIn: parent
            text: isPlaying ? i18n.tr("Stop playing") : i18n.tr("Play selected")
            visible: getFileName(temp_sound) != "No sound"
            onClicked: isPlaying ? audio_play.stop() : audio_play.play()
        }

        trailingActionBar.actions: Action {
            text: "confirm"
            iconName: "ok"
            onTriggered: {
                audio_play.stop()
                timersound = temp_sound
                if (getFileName(timersound) == "No sound") {
                    audio_play.muted = true
                } else {
                    audio_play.muted = false
                }
                sound_ldr.source = ""
            }
        }
    }

    Audio {
        id: audio_play

        source: Qt.resolvedUrl(temp_sound)
        loops: 2
        audioRole: Audio.AlarmRole
    }

    Sections {
        id: category_sections
        width: parent.width  // needed, otherwise the sections are not horizontally swipeable
        StyleHints {selectedSectionColor: top_text_color; }
        anchors {
            top: main_header.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        model: [i18n.tr("Timer sounds"), i18n.tr("System sounds"), i18n.tr("Custom sounds")]
    }

    ListView {
        id: timersound_listview
        visible: category_sections.selectedIndex === 0
        width: parent.width
        height: root_soundpickerpage.height - (main_header.height + category_sections.height + units.gu(3))
        anchors.top: category_sections.bottom
        anchors.topMargin: units.gu(2)
        clip: true

        FolderListModel {
            id: appSoundModel
            nameFilters: [ "*.ogg"]
            folder: appSoundFolder
            showDirs: false
        }

        model: appSoundModel

        delegate:
            ListItem {
            id: timer_sound_item

            height: timer_main_layout.height
            divider.visible: false

            ListItemLayout {
                id: timer_main_layout
                title.text: fileIsDir ? "" : fileBaseName != "No sound" ? fileBaseName : i18n.tr("No sound")

                Icon {
                    name: "tick"
                    color: timer_main_layout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Trailing
                    opacity: filePath == temp_sound ? 1 : 0
                }
            }
            onClicked: {
                temp_sound = filePath
                audio_play.source = temp_sound
            }
        }
    }

    ListView {
        id: systemsound_listview
        visible: category_sections.selectedIndex === 1
        width: parent.width
        height: root_soundpickerpage.height - (main_header.height + category_sections.height + units.gu(3))
        anchors.top: category_sections.bottom
        anchors.topMargin: units.gu(2)
        clip: true

        FolderListModel {
            id: systemSoundModel
            nameFilters: [ "*.ogg", "*.mp3" ]
            folder: systemSoundFolder
            showDirs: false
        }

        model: systemSoundModel

        delegate:
            ListItem {
            id: system_sound_item

            height: system_main_layout.height
            divider.visible: false

            ListItemLayout {
                id: system_main_layout
                title.text: !fileIsDir ? fileBaseName : ""

                Icon {
                    name: "tick"
                    color: system_main_layout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Trailing
                    opacity: filePath == temp_sound ? 1 : 0
                }
            }
            onClicked: {
                temp_sound = filePath
                audio_play.source = temp_sound
            }
        }
    }

    ListView {
        id: customsound_listview
        visible: category_sections.selectedIndex === 2
        width: parent.width
        height: root_soundpickerpage.height - (main_header.height + category_sections.height + units.gu(3))
        anchors.top: category_sections.bottom
        anchors.topMargin: units.gu(2)
        clip: true

        FolderListModel {
            id: customSoundModel
            nameFilters: [ "*.ogg", "*.mp3" ]
            folder: customSoundFolder
            showDirs: false
        }

        model: customSoundModel

        delegate: 
            ListItem {
                id: custom_sound_item
                visible: customSoundModel.count > 0 ? true : false
                height: custom_main_layout.height
                divider.visible: false

                ListItemLayout {
                    id: custom_main_layout
                    title.text: !fileIsDir ? fileBaseName : ""

                    Icon {
                        name: "tick"
                        color: custom_main_layout.title.color
                        width: units.gu(2)
                        height: width
                        SlotsLayout.position: SlotsLayout.Trailing
                        opacity: filePath == temp_sound ? 1 : 0
                    }
                }
                onClicked: {
                    temp_sound = filePath
                    audio_play.source = temp_sound
                }
            }
            Column {
                id: noCustomSoundInfo
                visible: customSoundModel.count > 0 ? false : true //filePath == "/opt/click.ubuntu.com/timer.danfro/2.2.3/share" ? true : false //returned by FolderListModel if custom sound folder does not exist
                width: parent.width
                height: units.gu(20)
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: units.gu(2)
                    rightMargin: units.gu(2)
                }
                spacing: units.gu(1)
                Label {
                    text: i18n.tr("No custom sound found. To use your own sound files (*.ogg, *.mp3), copy them in this folder:")
                    wrapMode: Text.WordWrap
                    width: parent.width
                }
                Label {
                    text: "~/.local/share/timer.danfro/customSounds"
                    font.italic: true
                    wrapMode: Text.WordWrap
                    width: parent.width
                }
            }
    }
}
