v2.3.0
- added: option to use custom sounds from ~/.local/share/timer.danfro/customSounds folder
- updated: translations, many thanks to all translators!

v2.2.3
- updated: translations, many thanks to all translators!

v.2.2.2
- improved: hide play sound button when "no sound" is selected
- improved: when selection a sound, automatically stop playing sounds after 2 loops
- improved: load app sounds from folder instead of a hardcoded list
- fixed: reading and playing of system sound files (was broken)
- updated: translations, many thanks to all translators!

v2.2.1
- improved: use UBports Hosted Weblate for translation services, thanks to Weblate for providing us with a free service
- updated: translations and added several languages, many thanks to all translators!

v2.2.0
- added: port app to run on focal

v2.1.1
- improved: make endtime of timers display locale format with/without am/pm notation
- updated: french translation, thanks Anne Onyme 017

v2.1.0
- added: Norwegian translation, thanks to JanC Sherdahl
- added: Dutch translation, thanks to Heimen Stoffels
- added: option for "no sound" timers
- improved: refactor about page (add links to source and changelog, delete some broken links, internal redesign, remove dividers)
- improved: replace all hardcoded colors with themed colors (one exception kept)
- fixed: system sound list not displaying the last few entries
- removed: dividers in several places
- improved: reformat some strings to allow better translations by using .arg()
- updated: framework to 16.04.4

v2.0.5
- improved: move tick icon for all settings to the right side
- removed: temporary remove link for translate-ut.org
- removed: link to SwipeToBack poll
- fixed: swipeDistance not showing default value on first start
- fixed: SwipeToBack not working on timer details page

v.2.0.4
- improved: translation now available via community weblate on translate-ut.org
- improved: redesign of SwipeToBack
  * SwipeToBack now always works in both directions (left and right)
  * back click/tap and SwipeToBack are both enabled to allow convergent use with touch and mouse
  * use swipetoback icon instead of back icon (can be clicked/tapped as before)
- updated: translations, thanks to all translators

v2.0.3
- updated: spanish translation, thanks Krakakanok
- improved: small changes to first time notes on adding favourites
- improved: all setting strings now start with capital letter

v2.0.2
- improved: about page now only flickable if needed

v2.0.1
- updated: french translation, thanks Anne Onyme 017

v2.0.0
- added: swipe to action feature
- improved: respect system theme (now under settings) and adjust colours to be set according to theme
- added: system ringtones
- added: option for close button on main page
- improved: partially redesign of about page
- improved: implement clickable kill command

v1.3.2
- improved: buttons now always visible

v1.3.1
- updated: small translation updates
- improved: change version to 3 digits to allow semantic versioning

v1.3
- added: important information to about page
- added: french translation thanks to Anne Onyme 017
- added: spanish translation, thanks to Krakakanok
- improved: increased button size

v1.2
- improved: new maintainer danfro
- improved: moved source code from launchpad to gitlab
- improved: allow timers up to 24 hours

v1.1
- improved: build for 16.04 framework
- last version based on launchpad code

v1.0
- added: allow multiple timers
